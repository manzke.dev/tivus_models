import uuid
from datetime import datetime
from sqlalchemy import Column, String, Boolean, Integer, DateTime, ForeignKey
from sqlalchemy.orm import declarative_base, relationship
from sqlalchemy.dialects.postgresql import UUID
from pydantic import BaseModel, constr

Base = declarative_base()


class AgenteModel(BaseModel):
    id: int
    conta_id: uuid.UUID
    nome: constr(max_length=128)
    is64bits: bool
    username: constr(max_length=64)
    arquitetura: int

    class Config:
        orm_mode = True


class Agente(Base):
    __tablename__ = 'agente'
    id = Column(Integer, primary_key=True)
    conta_id = Column(UUID(as_uuid=True))
    nome = Column(String(128), nullable=False)
    is64bits = Column(Boolean, default=True)
    username = Column(String(64))
    arquitetura = Column(Integer, default=64)
    aplicativos = relationship('Aplicativo', secondary='agente_aplicativo', back_populates='agentes', cascade='all, delete')
    grupos = relationship('Grupo', secondary='grupo_agente', back_populates='agentes', cascade='all, delete')
    historicos = relationship('Historico', cascade='all, delete')

    def model(self):
        return AgenteModel.from_orm(self)


class AgenteAplicativo(Base):
    __tablename__ = 'agente_aplicativo'
    id = Column(Integer, primary_key=True)
    agente_id = Column(Integer, ForeignKey('agente.id'))
    aplicativo_id = Column(Integer, ForeignKey('aplicativo.id'))
    versao = Column(String, nullable=False)
    data = Column(DateTime, nullable=False, default=datetime.now())
    # agente = relationship("Agente", back_populates="aplicativo")
    # aplicativo = relationship("Aplicativo", back_populates="agente")


class Aplicativo(Base):
    __tablename__ = 'aplicativo'
    id = Column(Integer, primary_key=True)
    conta_id = Column(UUID(as_uuid=True))
    nome = Column(String, nullable=False, default="Novo Aplicativo")
    comando = Column(String, nullable=False, default="comando.exe")
    argumentos = Column(String)
    versao = Column(String, nullable=False, default="1.0")
    exit_code = Column(String, nullable=False, default="0")
    forcar = Column(Boolean, default=False)
    descricao = Column(String)
    habilitado = Column(Boolean, default=False)
    process_name = Column(String)
    arquitetura = Column(Integer, default=64)
    usuario = Column(Boolean, default=False)
    agentes = relationship('Agente', secondary='agente_aplicativo', back_populates='aplicativos', cascade='all, delete')
    usuarios = relationship('Usuario', secondary='usuario_aplicativo', back_populates='aplicativos', cascade='all, delete')
    grupos = relationship('Grupo', secondary='grupo_aplicativo', back_populates='aplicativos', cascade='all, delete')


class Config(Base):
    __tablename__ = 'config'
    id = Column(Integer, primary_key=True)
    nome = Column(String, nullable=False)
    valor = Column(String, nullable=False)

    def __repr__(self):
        return f"<Config(nome='{self.nome}', valor='{self.valor}')>"


class Conta(Base):
    __tablename__ = 'conta'
    id = Column(UUID(as_uuid=True), primary_key=True, default=uuid.uuid4)
    dominio = Column(String)
    dominio_usuario = Column(String)
    dominio_senha = Column(String)


class Grupo(Base):
    __tablename__ = 'grupo'
    id = Column(Integer, primary_key=True)
    conta_id = Column(UUID(as_uuid=True))
    nome = Column(String(50), nullable=False)
    auto_agente = Column(Boolean, default=False, nullable=False)
    agentes = relationship('Agente', secondary='grupo_agente', back_populates='grupos', cascade='all, delete')
    aplicativos = relationship('Aplicativo', secondary='grupo_aplicativo', back_populates='grupos', cascade='all, delete')
    usuarios = relationship('Usuario', secondary='grupo_usuario', back_populates='grupos', cascade='all, delete')


class GrupoAgente(Base):
    __tablename__ = 'grupo_agente'
    id = Column(Integer, primary_key=True)
    grupo_id = Column(Integer, ForeignKey('grupo.id'), nullable=False)
    agente_id = Column(Integer, ForeignKey('agente.id'), nullable=False)


class GrupoAplicativo(Base):
    __tablename__ = 'grupo_aplicativo'
    id = Column(Integer, primary_key=True)
    grupo_id = Column(Integer, ForeignKey('grupo.id'), nullable=False)
    aplicativo_id = Column(Integer, ForeignKey('aplicativo.id'), nullable=False)


class GrupoUsuario(Base):
    __tablename__ = 'grupo_usuario'
    id = Column(Integer, primary_key=True)
    grupo_id = Column(Integer, ForeignKey('grupo.id'), nullable=False)
    usuario_id = Column(Integer, ForeignKey('usuario.id'), nullable=False)


class Historico(Base):
    __tablename__ = 'historico'
    id = Column(Integer, primary_key=True)
    agente_id = Column(Integer, ForeignKey('agente.id'))
    usuario_id = Column(Integer, ForeignKey('usuario.id'))
    data = Column(DateTime, nullable=False, default=datetime.now())
    texto = Column(String)
    tipo = Column(Integer, nullable=False, default=0)


class Usuario(Base):
    __tablename__ = 'usuario'
    id = Column(Integer, primary_key=True)
    conta_id = Column(UUID(as_uuid=True))
    username = Column(String(64), nullable=False)
    grupos = relationship('Grupo', secondary='grupo_usuario', back_populates='usuarios', cascade='all, delete')
    aplicativos = relationship('Aplicativo', secondary='usuario_aplicativo', back_populates='usuarios', cascade='all, delete')
    historicos = relationship('Historico',  cascade='all, delete')


class UsuarioAplicativo(Base):
    __tablename__ = 'usuario_aplicativo'
    id = Column(Integer, primary_key=True)
    usuario_id = Column(Integer, ForeignKey('usuario.id'), nullable=False)
    aplicativo_id = Column(Integer, ForeignKey('aplicativo.id'), nullable=False)
    versao = Column(String, nullable=False)
    data = Column(DateTime, nullable=False, default=datetime.now())
