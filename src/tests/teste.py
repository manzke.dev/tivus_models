import uuid

from src.tivus_models.orm import Agente


def main():
    agente = Agente()
    agente.nome = 'teste'
    agente.id = 0 #uuid.uuid4()
    agente.conta_id = uuid.uuid4()
    agente.is64bits = False
    agente.username = ''
    agente.arquitetura = 32
    print(agente.model().json())


if __name__ == "__main__":
    main()
